# Extracción y reproducción audio de YouTube desde terminal Linux

## Lab 2 - Unidad II - SSOO y redes

El presente programa presenta una serie de procesos que permiten la descarga de un video, la extracción de su audio y la reproducción de este al dar un URL de un video de la plataforma de YouTube. Dada la ejecución del programa y la URL deseada todos los procesos nombrados se realizan de manera automática. Todo se realiza con una serie de programas adicionales: youtube-dl y sox.

## Comenzando

Youtube-dl es un programa que dada una o más url permite descargar un video de youtube con un conjunto de condiciones y extracciones adicionales dependiendo de los comandos entregados. Esto se hace mediante una terminal de Linux. Sox es un programa que permite la reproducción de audios desde la terminal. 

Los procesos son creados con la función **fork()** de C++, el cual permite un proceso hijo descargue un video y extraiga el audio, mientras que su proceso padre reproduce dicha extracción. Se debe agregar un URL al programa y el correspondiente, ya que si no existe no va a funcionar y se debe reiniciar.

## Prerrequisitos

### Sistema operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

Siempre para instalar o correr programas se recomienda actualizar su máquina mediante el comando desde la terminal `sudo apt update && sudo apt upgrade`.

### Make

El programa cuenta con un archivo de compilación Make (Makefile). Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

**En caso de no usar make**

Se puede ejecutar el programa sin make (revisar **Ejecutando las pruebas**).

### [Youtube-dl](http://rg3.github.io/youtube-dl/download.html) 

El programa para realizar los procesos necesita de la instalación de youtube-dl. Sin este programa, nuestros procesos no funcionarán. Se pueden instalar de diversas maneras:

**Usando curl**

`sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl`

`sudo chmod a+rx /usr/local/bin/youtube-dl`

De igual manera se puede instalar el programa curl mediante el siguiente comando `sudo apt  install curl`. Presenta diversas versiones por lo que para revisar la versión instalada se puede ocupar `curl --version`.

**Sin curl**

En el caso de no tener curl se puede instalar youtube-dl con la siguiente serie de comandos:

`sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl`

`sudo chmod a+rx /usr/local/bin/youtube-dl`.

**Usando pip**

`sudo pip install --upgrade youtube_dl`

**Para extraer audio**

Para las funciones de youtube-dl de extracción de audio se necesitan ciertos paquetes los cuales el programa sugiere a la hora de instalar: _ffmpeg/avconv and ffprobe/avprobe_. Estos se instalan bajo el siguiente comando: `sudo apt-get install ffmpeg`.

### [SOX](https://ubunlog.com/sox-reproduce-mp3-terminal/#Reproducir_archivos_mp3_usando_Sox) 

Para que posterior a la extracción del audio este se pueda reproducir desde la misma terminal de manera automática se necesita del programa SOX, el cual debe ser instalado de la siguiente manera:

`sudo apt install sox`

Para poder reproducir todo tipo de archivos de audio se necesita instalar una librería para sox con el siguiente comando: 

`sudo apt-get install libsox-fmt-all`

## Ejecutando las pruebas

### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ programa.cpp -o programa`

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando con parámetro de entrada:

`./programa {URL}`

Donde URL es el link del video de youtube que se desea descargar y extraer el audio. Si no se ingresa dicho URL el programa no inciará y si se agrega más de uno solo se descargará el primer video dado.

El programa posteriormente dará el ID del proceso hijo encargado de la descarga, el cual se da por youtube-dl. El video se descarga en .mp4 y el audio de este en formato .mp3 bajo el nombre de "song.mp3".

Cuando el audio finalmente se extraiga el proceos padre finalmente reproducirá el audio con el nombre "song.mp3". Recordar que todo esto debería hacerse en una carpeta específica para el programa, donde se compila el código con la carpeta descargada con todos los archivos. Si ya existe un archivo con el nombre con que se descarga el audio este será reemplazado por el hecho por este proceso. Todo esto también tendrá su propio ID. 

La salida forzosa del programa en cualquier parte de su ejecución es mediante `ctl+z`o `ctl+c`.

## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos). La rapidez de descarga depende únicamente del internet bajo el cual se ejecute. 

## Construido con
- Lenguaje c++: librería iostream, unistd.h, sys/wait.h


## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl

