#include <unistd.h>
#include <sys/wait.h>
#include <iostream>

// para cadenas de caracteres y string
using namespace std;

class Fork {
  /*atributos privados:
    pid (process id)
    segundos de espera entre proceso padre e hijo
    link del video a descargar
  */
  private:
    pid_t pid;
    int segundos;
    char* link;

  public:
    // constructor de clase que recibe segundos de espera en download como link de video a descargar
    Fork (int seg, char* download) {
      segundos = seg;
      link = download;
      // se llama a parámetros para los procesos
      crear();

    }

    // métodos
    void crear() {
      // con fork se crea el proceso hijo, el cual retorna un número identificando si se pudo crear
      pid = fork();
    }

    void procesoPadre(){

      cout << "Continua con código proceso padre: " << getpid() << endl;
      // programa sox puede reproducir dicho audio descargado -v para volumen
      sleep(2);
      execlp("sox", "play", "-v", "1", "song.mp3", NULL);
      //PlaySound("sound.mp3", NULL, SND_ASYNC);
      cout << "Término padre" << endl;
    }

    void procesoHijo(){
      // se ejecuta el proceso hijo

      cout << "Ejecutando proceso hijo, Extrae audio ID:" << getpid() << endl;

      /*Comandos con variable PATH (excelp)
        comandos de acuerdo con youtube-dl para extracción de audio con extensión mp3
        -o song.mp3, .mp3 dado por el comando %(ext)s, es para definir el nombre del audio para posterior reproducción
      */

      execlp("youtube-dl", "youtube-dl", "-x", "--audio-format",
              "mp3", "-o", "song.%(ext)s", link, NULL);

      // espera cierta cantidad de segundos antes de continuar
      sleep(segundos);
      cout << "Termina proceso hijo" << endl;
    }

    void ejecutarCodigo () {
      // se valida la creación del subproceso
      // si es menor a 0 entonces no es válido y no se pudo crear
      if (pid < 0) {

        cout << "El proceso no se ha podido crear" << endl;
        // si es igual a 0
      } else if (pid == 0) {

        procesoHijo();

      } else {
        // espera que proceso hijo finalice
        wait (NULL);

        procesoPadre();
      }
    }
};

// main
int main(int argc, char *argv[])
{
  // parámetro de entrada, link de video de youtube a descargar
  char* link = argv[1];

  // si no ingresa nada entonces es NULL. Si ingresa algo entonces el programa prosigue
  if(link!=NULL){
    // Llamada clase para instanciación de proceso, retornando su id
    Fork fork(3, link);

    //Procesos corriendo
    fork.ejecutarCodigo();
  }else{
    cout << "No ingresó nada. Debe ingresar un URL, intentelo otra vez." << endl;
  }

  return 0;
}
